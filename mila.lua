--[[
	Mobs Wolf - Adds wolves.
	Copyright © 2018, 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the 'Licence');
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	'AS IS' basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization

local S = minetest.get_translator('mobs_wolf')


--
-- Wolf entity
--

mila:add_entity('mobs_wolf:wolf', {
	sounds = 'mobs_wolf_spawn',
	physical = true,
	collide_with_objects = true,
	textures = {'mobs_wolf.png'},
	mesh = 'mobs_wolf.b3d',
	damage = 5,
	range = 1,
	status = 'hostile',
	makes_footstep_sound = true,
	stepheight = 1.1,
	collisionbox = {-0.4, -0.6, -0.4, 0.4, 0.3, 0.4},
	visual_size = {x = 1, y = 1},
	visual = 'mesh',
	automatic_face_movement_dir = 0,
	hp_max = minetest.PLAYER_MAX_HP_DEFAULT,
	drops = 'mila:steak',
	speed = 4,
	view_range = 10
})


--
-- Spawning
--

mila:add_spawn('mobs_wolf:wolf',{
	nodenames = {'air'},
	neighbors = {
		'default:dirt_with_grass',
		'default:dirt_with_snow',
		'default:dirt_with_coniferous_litter'
	},
	interval = 45,
	chance = 7500  -- 0.013%
})

-- Spawn Egg

mila:add_egg('mobs_wolf:wolf', {
	description = S('Spawn Wolf'),
	inventory_image = 'default_grass.png'
})


--
-- Minetest engine debug logging
--

local s_LogLevel = minetest.settings:get('debug_s_LogLevel')

if (s_LogLevel == nil)
or (s_LogLevel == 'action')
or (s_LogLevel == 'info')
or (s_LogLevel == 'verbose')
then
	s_LogLevel = nil
	minetest.log('action', '[Mod] Mobs Wolf [v0.4.0] loaded.')
end
