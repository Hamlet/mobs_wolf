--[[
	Mobs Wolf - Adds wolves.
	Copyright © 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization

local S = minetest.get_translator('mobs_wolf')


--
-- Wolf entity
--

creatures.register_mob('mobs_wolf:wolf', {
	stats = {
		hp = minetest.PLAYER_MAX_HP_DEFAULT,
		breath = minetest.PLAYER_MAX_BREATH_DEFAULT,
		hostile = true,
		lifetime = 1200, -- seconds, or 20 minutes (default MTG's day length).
		can_swim = true,
		can_fly = false,
		can_panic = false,
		has_knockback = true,
		sneaky = false,
		max_drop = 3,
		has_falldamage = true,
		can_jump = 1
	},

	modes = {
		idle = {chance = 0.2, duration = 5},
		walk = {chance = 0.5, duration = 15, moving_speed = 2},
		attack = {chance = 0.3, duration = 20, moving_speed = 5.2}
	},

	model = {
		mesh = 'mobs_wolf.b3d',
		textures = {'mobs_wolf.png'},
		collisionbox_width = 0.8,
		collisionbox_height = 0.9,
		center_height = -0.6,
		rotation = 0.0,
		scale = {x = 1.0, y = 1.0},
		backface_culling = true,
		vision_height = 0,
		weight = 40,
		animations = {
			idle = {start = 1, stop = 60, speed = 20, loop = true},
			walk = {start = 61, stop = 120, speed = 65, loop = true},
			attack = {start = 61, stop = 120, speed = 65, loop = true}
		}
	},

	sounds = {
		on_damage = {name = 'mobs_wolf_spawn', gain = 1.0, distance = 10},
		swim = {name = 'creatures_splash', gain = 1.0, distance = 10,},
		random = {
			idle = {name = 'mobs_wolf_spawn', gain = 1.0, distance = 10},
			attack = {name = 'mobs_wolf_hit', gain = 1.0, distance = 10}
		}
	},

	drops = {
		{
			'creatures:flesh',
			{min = 1, max = 2},
			chance = 0.25
		}
	},

	combat = {
		attack_damage = 5,
		attack_speed = 1.0,
		attack_radius = 1.5,
		search_enemy = true,
		search_timer = 5,
		search_radius = 10,
		search_type = 'nonhostile',
	},

	spawning = {
		ambience = {
			spawn_type = 'abm',
			spawn_zone_width = 100,
			max_number = 2,
			number = 1,
			height_limit = {min = -10, max = 200},

			abm_interval = 45,
			abm_chance = 7500, -- 0.013%
			abm_nodes = {
				spawn_on = {
					'default:dirt_with_grass',
					'default:dirt_with_snow',
					'default:dirt_with_coniferous_litter'
				},

				neighbors = {'air'}
			}
		},

		spawn_egg = {
			description = S('Spawn Wolf'),
			texture = 'default_grass.png'
		}
	}
})


--
-- Minetest engine debug logging
--

local s_LogLevel = minetest.settings:get("debug_s_LogLevel")

if (s_LogLevel == nil)
or (s_LogLevel == "action")
or (s_LogLevel == "info")
or (s_LogLevel == "verbose")
then
	s_LogLevel = nil
	minetest.log("action", "[Mod] Mobs Wolf [v0.4-0] loaded.")
end
