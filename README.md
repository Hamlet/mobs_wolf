### Mobs Wolf
![Mobs Wolf's screenshot](screenshot.png)  
**_Adds wolves._**

**Version:** 0.4-0  
**Source code's license:** [EUPL v1.2][1] or later.  
**Media (Textures, Models, Sounds) license:** [CC-BY-SA 3.0][2]

**Dependencies:** default (found in [Minetest Game][5])  
**Mobiles API required:** [M.I.L.A.][6] or [Mob-Engine][7] or [Mobs Redo][8]

Texture, model and sounds are from [Voxelands][3].

To satisfy the attribution clause, any project using Voxelands' assets  
must provide a prominent notice as part of each of said project's  
credits notices (such as in documentation, on a website, and/or any  
other credits screen associated with the project) showing both the  
CC-BY-SA licensing, the ownership by Voxelands of the asset, and a link  
to the Voxelands' project at [http://www.voxelands.com/][4].

Artists include:  
sdzen  
darkrose  
sapier  
Tom Peter  
Telaron  
juskiddink  
With special thanks to http://www.opengameart.org/


### Installation

Unzip the archive, rename the folder to mobs_wolf and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods



[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/licenses/by-sa/3.0/
[3]: https://gitlab.com/voxelands/voxelands
[4]: https://web.archive.org/web/20171019224039/https://www.voxelands.com/
[5]: https://github.com/minetest/minetest_game
[6]: https://forum.minetest.net/viewtopic.php?t=15375
[7]: https://forum.minetest.net/viewtopic.php?t=22429
[8]: https://forum.minetest.net/viewtopic.php?t=9917
