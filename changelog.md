# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/).


## [Unreleased]

	- No further features planned.



## [0.4.0] - 2019-12-15
### Added

	- Support for Mob-Engine.

### Changed

	- Minor code tweaks.



## [0.3.1] - 2019-11-08 (Unreleased)
### Changed

	- Minor code improvements.



## [0.3.0] - 2019-10-18
### Added

	- Support for translations.

### Changed

	- License changed to EUPL v1.2.
	- mod.conf set to follow MT v5.x specifics.
	- Textures have been optimized (with optipng).
	- Spawn now occurs on dirt_with_grass, dirt_with_snow, dirt_with_coniferous_litter.

### Removed

	- Support for MT v0.4.x



## [0.2.1] - 2018-07-28
### Changed

	- Wolves will be peaceful during daytime.
	- Fixed wrong "floats" setting.
	- Minor code improvements.



## [0.2.0] - 2018-07-12
### Added

	- Support for MILA API.

### Changed

	- Spawn chance increased to 5000 (was 3000)



## [0.1.0] - 2018-07-10
### Added

	- Initial release.
