--[[
	Mobs Wolf - Adds wolves.
	Copyright © 2018, 2019 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the 'Licence');
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	'AS IS' basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Used for localization

local S = minetest.get_translator('mobs_wolf')


--
-- Wolf entity
--

mobs:register_mob('mobs_wolf:wolf', {
	type = 'monster',
	hp_min = minetest.PLAYER_MAX_HP_DEFAULT,
	hp_max = minetest.PLAYER_MAX_HP_DEFAULT,
	armor = 100,
	walk_velocity = 2,
	run_velocity = 5.2,
	walk_chance = 2, -- 50%
	stand_chance = 3, -- 33%
	jump = true,
	jump_height = 1.1,
	stepheight = 1.1,
	pushable = false,
	view_range = 10,
	damage = 5,
	knock_back = true,
	fear_height = 3,
	lava_damage = minetest.PLAYER_MAX_HP_DEFAULT,
	suffocation = true,
	floats = 1,
	reach = 1,
	docile_by_day = true,
	attack_chance = 4, -- 25%
	attack_monsters = true,
	attack_animals = true,
	attack_players = true,
	group_attack = true,
	attack_type = 'dogfight',
	pathfinding = 1,
	makes_footstep_sound = true,
	sounds = {
		random = 'mobs_wolf_spawn',
		attack = 'mobs_wolf_hit'
	},
	drops = {
		name = 'mobs:meat_raw',
		chance = 4, -- 25%
		min = 1,
		max = 2
	},
	visual = 'mesh',
	visual_size = {x = 1, y = 1},
	collisionbox = {-0.4, -0.6, -0.4, 0.4, 0.3, 0.4},
	selectionbox = {-0.4, -0.6, -0.4, 0.4, 0.3, 0.4},
	textures = {'mobs_wolf.png'},
	mesh = 'mobs_wolf.b3d',
	rotate = -90,
	animation = {
		stand_start = 1,
		stand_end = 60,
		walk_start = 61,
		walk_end = 120,
		walk_speed = 65
	}
})


--
-- Spawning
--

mobs:spawn({
	name = 'mobs_wolf:wolf',
	nodes = {
		'default:dirt_with_grass',
		'default:dirt_with_snow',
		'default:dirt_with_coniferous_litter'
	},
	neighbors = 'air',
	max_light = 15,
	min_light = 0,
	interval = 45,
	chance = 7500, -- 0.013%
	active_object_count = 2,
	min_height = -10,
	max_height = 300
})

-- Spawn Egg

mobs:register_egg('mobs_wolf:wolf', S('Spawn Wolf'), 'default_grass.png', 1)


--
-- Alias
--

mobs:alias_mob('mobs:wolf', 'mobs_wolf:wolf')


--
-- Minetest engine debug logging
--

local s_LogLevel = minetest.settings:get('debug_s_LogLevel')

if (s_LogLevel == nil)
or (s_LogLevel == 'action')
or (s_LogLevel == 'info')
or (s_LogLevel == 'verbose')
then
	s_LogLevel = nil
	minetest.log('action', '[Mod] Mobs Wolf [v0.4.0] loaded.')
end
